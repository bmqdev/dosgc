import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import CV from '../views/CV.vue'
import FindingXYZ from '../views/FindingXYZ.vue'
import FindingABC from '../views/FindingABC.vue'
import BestWay from '../views/BestWay.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/cv',
    name: 'cv',
    component: CV
  },
  {
    path: '/doscg',
    name: 'DOSCG',
    component: Home
  },
  {
    path: '/google',
    name: 'google',
    component: BestWay
  },
  {
    path: '/finding/xyz',
    name: 'finding-xyz',
    component: FindingXYZ
  },
  {
    path: '/finding/abc',
    name: 'finding-abc',
    component: FindingABC
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
