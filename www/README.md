# www

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Copy file ".env.example" and rename to ".env", Add params below 
```
VUE_APP_API_URL=http://localhost:_YOUR_API_PORT__/api

```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
