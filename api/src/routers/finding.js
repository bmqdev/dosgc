import express from 'express'
import redis from 'redis'

import { resp } from '../utils/resp'
import { controller } from '../controllers/controllers'

const redisClient = redis.createClient()
export const router = express.Router()
router.get('/abc', findingABC)
router.get('/xyz', findingXYZ)

export async function findingABC(req, res, next) {
  try {
    const key = 'findingABC'
    await redisClient.get(key, async (err, reply) => {
      if (reply) {
        next(resp({ data: JSON.parse(reply), source: 'cache' }))
      } else {
        const data = await controller.finding.findABC()
        redisClient.set(key, JSON.stringify(data))
        redisClient.expire(key, 360)
        next(resp({ data }))
      }
    });
  } catch (err) {
    next(resp({ message: err.message }, 400))
  }
}

export async function findingXYZ(req, res, next) {
  try {
    const key = 'findingXYZ'
    await redisClient.get(key, async (err, reply) => {
      if (reply) {
        next(resp({ data: JSON.parse(reply), source: 'cache' }))
      } else {
        const data = await controller.finding.findXYZ()
        redisClient.set(key, JSON.stringify(data))
        redisClient.expire(key, 360)
        next(resp({ data }))
      }
    });
  } catch (err) {
    next(resp({ message: err.message }, 400))
  }
}