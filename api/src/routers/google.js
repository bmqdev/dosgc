import express from 'express';
import { resp } from '../utils/resp'
import { controller } from '../controllers/controllers'

export const router = express.Router();
router.get('/bestway', bestway);

const ORI = 'SCG สำนักงานใหญ่ บางซื่อ 1 Siam Cement Alley, Bang Sue, Bangkok 10800';
const DES = 'Central World, 999/9 Rama I Rd, Pathum Wan, Pathum Wan District, Bangkok 10330';

export async function bestway(req, res, next) {
  try {
    const origin = req.query.origin || ORI;
    const destination = req.query.origin || DES;
    const data = await controller.google.bestway(origin, destination)
    next(resp({ data }));
  } catch (err) {
    next(resp({ message: err.message }, 400));
  }
}
