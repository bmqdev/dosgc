import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import { middleware } from '@line/bot-sdk'

import { router as finding } from './routers/finding'
import { router as line } from './routers/line'
import { router as google } from './routers/google'

import config from './config/config'

const PORT = 9000
const init = async () => {
  try {
    const app = express()
    app.use(cors())
    app.use('/api/finding', finding)
    app.use('/api/line/webhook', middleware(config.line))
    app.use('/api/line', line)
    app.use('/api/google', google)

    app.use(bodyParser.json())
    app.use(([body, status], req, res, next) => {
      res.status(status).json(body)
      next()
    })
    app.listen(PORT);
  } catch (e) {
    console.log(e)
  }
}

init()
