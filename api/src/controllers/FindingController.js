export class FindingController {
  findABC() {
    const A = 21;
    const res = {
      B: 23 - A,
      C: -21 + (-A),
    };
    return res
  }

  findXYZ() {
    const X = undefined
    const Y = undefined
    const Z = undefined
    const data = [X, Y, 5, 9, 15, 23, Z]
    const res = this.calculate(data)
    return res;
  }

  calculate(data) {
    let num = 0
    for (let i = 0; i < data.length; i++) {
      if (data[i] === undefined) {
        const next = data[i + 1]
        const prev = data[i - 1]
        switch (true) {
          case (next !== undefined):
            data[i] = next - num
            break
          case (prev !== undefined):
            data[i] = prev + (num - 2)
            break
          default:
            break
        }
      }
      num += 2;
    }
    if (data.filter(i => i === undefined).length > 0) {
      data = this.calculate(data)
    }
    return data
  }
}